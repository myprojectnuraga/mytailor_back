@extends('admin.layouts.app')
@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Администрация</a>
        </li>
        <li class="breadcrumb-item active">Меню</li>
    </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Список меню
                    <a href="{{route('menu.create')}}" class="float-right">Добавить</a>
                </div>
                <div class="card-block">
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Ссылка</th>
                            <th style="text-align: right">Редактирование</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($menu_item))
                            @foreach($menu_item as $item)
                                <tr class="item-{{$item->id}}">
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->title}}</td>
                                    <td>{{$item->link}}</td>
                                    <td style="text-align: right">
                                        <a href="{{route('menu.edit',['id'=>$item->id])}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil-square-o "></i></a>
                                        <button type="button" data-id="{{$item->id}}" class=" btn-trash btn btn-outline-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    @if(count($menu_item))
                        @include('admin.layouts.pagination', ['pagination' => $menu_item])
                    @endif
                </div>
            </div>
        </div>
        <!--/.col-->
    </div>
    <!--/.row-->
@stop
@push('script')
<script>
    $('.btn-trash').click(function(){
        var id = $(this).attr('data-id');
        $.ajax({
            url:"/admin/menu/"+id,
            type: "POST",
            data:{
                "_token" : "{{csrf_token()}}",
                "_method" : "DELETE",
                "id":id
            },success:function(data){
                $('.item-'+id).hide();
               if(data['status'] =='success'){
                   toastr.success(data['message']);

               }else{
                   toastr.error(data['message']);
               }

            }
        });

    });
</script>
@endpush