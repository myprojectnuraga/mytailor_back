@extends('admin.layouts.app')
@section('breadcrumb')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Администрация</a>
        </li>
        <li class="breadcrumb-item"><a href="{{route('category.index')}}">Категория</a></li>
        <li class="breadcrumb-item active">Добавление</li>
    </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Добавление категории
                    <a href="{{route('category.index')}}" class="float-right">Список</a>
                </div>
                <form action="{{route('category.store')}}" method="POST">
                    <div class="card-block">
                        <div class="row">
                            {{csrf_field()}}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name">Название</label>
                                    <input type="text" name="title" class="form-control" id="name" placeholder="введите название">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="type">Тип</label>
                                    <select id="type" name="type" class="form-control">
                                        <option value="0">Не выбрано</option>
                                        <option value="1">Основной</option>
                                        <option value="2">Для фильтра</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="type">Родитель категорий</label>
                                    <select id="type" name="parent_id" class="form-control">
                                        <option value="0">Не выбрано</option>
                                        @if(count($category))
                                            @foreach($category as $item)
                                                <option value="{{$item->id}}" >{{$item->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-secondary btn-sm">Сохранить</button>
                                <a href="{{route('category.index')}}" class="btn btn-secondary btn-sm">Отменить</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--/.col-->
    </div>
    <!--/.row-->
@stop
@push('script')
<script>
    @if(session('status') == 'error')
        toastr.error('{{session('message')}}');
    @endif
    @if($errors->any())
        @foreach($errors->all() as $error)
            toastr.error('{{$error}}');
    @endforeach
    @endif
</script>
@endpush
