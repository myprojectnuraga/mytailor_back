<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                Модули
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="/admin/menu"><i class="icon-puzzle"></i> Меню</a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="/admin/category"><i class="icon-puzzle"></i> Категория</a>
            </li>
        </ul>
    </nav>
</div>
