<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function getCategories()
    {
        $categories = Category::select(['title','type','parent_id','slug'])->get();
        if (count($categories)){
            $row['status'] = 200;
            $row['categories'] = $categories;
        }else{
            $row['status'] = 404;
            $row['categories'] = 'not found';
        }

        $result['result'] = $row;
        return response()->json($result);
    }
}
