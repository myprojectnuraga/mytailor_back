<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category_item = Category::orderBy('id','desc')->paginate(20);
        return view('admin.index.category',compact('category_item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::where('type',2)->orderBy('title','asc')->get();
        return view('admin.create.category',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'title'=>'required'
        ]);
        $category = new Category();
        $category->title = $request['title'];
        $category->type= $request['type'] ? $request['type'] : 0;
        $category->parent_id= $request['parent_id'] ? $request['parent_id'] : null;
        $category->slug= Helper::cyrToLat($request['title']);

        if ($category->save()){
            return redirect()->route('category.edit',$category->id)->with(['status'=> 'success','message'=>'Успешно сохранено']);
        }else{
            return redirect()->back()->with(['status'=>'error','message'=>'Ошибка при сохранений']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('type',2)->orderBy('title','asc')->get();
        $category_item = Category::find($id);
        return view('admin.edit.category',compact('category_item','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required'
        ]);
        $category = Category::find($id);
        $category->title = $request['title'];
        $category->type= $request['type'] ? $request['type'] : 0;
        $category->parent_id = $request['parent_id'] ? $request['parent_id'] : 0;
        $category->slug= Helper::cyrToLat($request['title']);

        if ($category->save()){
            return redirect()->route('category.edit',$category->id)->with(['status'=> 'success','message'=>'Успешно сохранено']);
        }else{
            return redirect()->back()->with(['status'=>'error','message'=>'Ошибка при сохранений']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::where('id',$id)->delete();
        $result['status'] = 'success';
        $result['message'] = 'Успешно удалено';

        return $result;
    }
}
