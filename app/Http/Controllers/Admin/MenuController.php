<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu_item = Menu::orderBy('id','desc')->paginate(20);
        return view('admin.index.menu',compact('menu_item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.create.menu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|max:255'
        ]);

        $menu_item = new Menu();
        $menu_item->title = $request['title'];
        $menu_item->link = $request['link'];

        if ($menu_item->save()){

            return redirect()->route('menu.edit',$menu_item->id)->with(['status'=> 'success','message'=>'Успешно сохранено']);

        }else{

            return redirect()->back()->with(['status'=>'error','message'=>'Ошибка при сохранений']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu_item = Menu::find($id);
        return view('admin.edit.menu',compact('menu_item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required|max:255'
        ]);

        $menu_item = Menu::find($id);
        $menu_item->title = $request['title'];
        $menu_item->link = $request['link'];

        if ($menu_item->save()){

            return redirect()->route('menu.edit',$menu_item->id)->with(['status'=> 'success','message'=>'Успешно сохранено']);

        }else{

            return redirect()->back()->with(['status'=>'error','message'=>'Ошибка при сохранений']);
        }    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::where('id',$id)->delete();
        $result['status'] = 'success';
        $result['message'] = 'Успешно удалено';

        return $result;
    }
}
