<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace'=>'Index','middleware'=>'web'],function ()
{
    Route::get('logout',function (){Auth::logout(); return redirect('/');});
    Route::get('login','AuthController@getLogin');
    Route::post('login','AuthController@postLogin');

});

Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware'=>'admin'],function ()
{
    Route::get('/','AdminController@index');
    Route::resource('menu','MenuController');
    Route::resource('user','UserController');
    Route::resource('category','CategoryController');
});